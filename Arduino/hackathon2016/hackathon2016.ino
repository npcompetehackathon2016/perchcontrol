/* Hackathon 2016
 *  NP Compete
 */
 

#include "pinlayout.h"
#include "melody.h"
#include "button.h"
#include "centerservo.h"
#include "colorsensor.h"


bool f_fStartupComplete = false;

uint8_t f_nChannelLeft = 0;
CColorSensor f_colorsensorLeft;
SColorReading f_colorreadingLeft;

uint8_t f_nChannelRight = 1;
CColorSensor f_colorsensorRight;
SColorReading f_colorreadingRight;

void setup() 
{
  //servo
  CCenterServo::vSetupCenterServo();

  //Button
  pinMode(gsc_pinPushButton, INPUT_PULLUP);
  pinMode(gsc_pinPushButtonLED, OUTPUT);
   
  //Serial
  Serial.begin(115200);
  Serial.setTimeout(100);

  f_colorsensorLeft.vSetup(f_nChannelLeft);
  f_colorsensorRight.vSetup(f_nChannelRight);
}

void loop()
{
  if (!f_fStartupComplete)
  {
    vUpdateStartup();
  }

  f_colorsensorLeft.vGetColorReadingHex(&f_colorreadingLeft);
  f_colorsensorRight.vGetColorReadingHex(&f_colorreadingRight);
  
  CButton::vUpdateButtonState();
  
  CCenterServo::vUpdateCenterServo(&f_colorreadingLeft, &f_colorreadingRight);
}

void serialEvent()
{
  while (Serial.available())
  {
    char t_chHeader = (char)Serial.read();
    
    if (t_chHeader == 's')
    {
      int t_nServo = Serial.parseInt();
      CCenterServo::vSetDesired(t_nServo);
    }
  }
}

void vUpdateStartup()
{
  vPlayMelody1();
  f_fStartupComplete = true;
  CButton::vSetButtonBrightness(CButton::msc_nButtonBrightOn);
}




