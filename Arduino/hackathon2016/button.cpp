#include "pinlayout.h"
#include "button.h"
#include "centerservo.h"
#include "melody.h"

//Button

int CButton::ms_nButtonState = LOW;
int CButton::ms_nButtonBrightCur = 0;

static const int fsc_nMilliPerChange = 80;
int f_nMilliLastChange = 0;
int f_dLedCur = 1;

void CButton::vUpdateButtonState()
{
  int t_nButtonNew = digitalRead(gsc_pinPushButton);
  if (ms_nButtonState != t_nButtonNew)
  {
    vSetButtonBrightness(msc_nButtonBrightOn);
    
    if (t_nButtonNew == HIGH)
    {
      CCenterServo::vMoveServoTo(CCenterServo::msc_nServoMax / 2); 
      vPlayMelody2();
      f_nMilliLastChange = millis();
    }
 
    ms_nButtonState = t_nButtonNew; 
    Serial.print("Button is : ");
    Serial.println(ms_nButtonState);
  }

  if (ms_nButtonState == HIGH)
  {
    int t_nMillisCur = millis();
    if (t_nMillisCur - f_nMilliLastChange > fsc_nMilliPerChange || t_nMillisCur < f_nMilliLastChange)
    {
      f_nMilliLastChange = t_nMillisCur;
      ms_nButtonBrightCur += f_dLedCur;
      if (ms_nButtonBrightCur > msc_nButtonBrightOff)
      {
        ms_nButtonBrightCur = msc_nButtonBrightOff;
        f_dLedCur = -f_dLedCur;
      }
      else if (ms_nButtonBrightCur < 0)
      {
        ms_nButtonBrightCur = 0;
        f_dLedCur = -f_dLedCur;
      }
    }
    vSetButtonBrightness(ms_nButtonBrightCur);
  }
}

void CButton::vSetButtonBrightness(int p_nBrightness)
{
  int t_n = constrain(p_nBrightness, 0, 255);
  ms_nButtonBrightCur = t_n;
  analogWrite(gsc_pinPushButtonLED, ms_nButtonBrightCur);
}

