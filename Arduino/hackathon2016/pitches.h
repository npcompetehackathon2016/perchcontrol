#ifndef _HACKATHON_2016_PITCHES_H_
#define _HACKATHON_2016_PITCHES_H_

#define NOTE_D5  587
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740

#define NOTE_MIN 31
#define NOTE_MAX 65535

#endif
