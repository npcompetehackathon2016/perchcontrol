#ifndef _HACKATHON_2016_CENTER_SERVO_H_
#define _HACKATHON_2016_CENTER_SERVO_H_

#include <Arduino.h>

struct SColorReading;
class CCenterServo
{
  public:
  static const int msc_nServoMin = 0;
  static const int msc_nServoMax = 180;  
  
  static void vSetupCenterServo();
  static void vUpdateCenterServo(SColorReading * p_pColorReadingLeft, SColorReading * p_pColorReadingRight);  
  static void vMoveServoTo(int p_nPos);

  inline static void vSetDesired(int p_nDesired) 
  {
    ms_nServoDesired = constrain(p_nDesired, msc_nServoMin, msc_nServoMax); 
  }  

  static int ms_nCenterPos;
  static int ms_nServoDesired;
};

#endif
