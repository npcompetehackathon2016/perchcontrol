#ifndef _HACKATHON_2016_MELODY_H_
#define _HACKATHON_2016_MELODY_H_

#include <Arduino.h>
#include "pitches.h"

void vPlayMelody1();
void vPlayMelody2();
void vPlayMelody(int p_cElements, int * p_paMelody, int * p_paTime);


#endif
