#include "centerservo.h"
#include "pinlayout.h"
#include "button.h"
#include "colorsensor.h"
#include <Servo.h>

//Servo
int CCenterServo::ms_nCenterPos = 90;
int CCenterServo::ms_nServoDesired = 90;

static const int fsc_nSpeed = 2;
static const int fsc_nMinRed = 215;
Servo f_servoCenter;

void CCenterServo::vSetupCenterServo()
{
    ms_nCenterPos = (msc_nServoMax - msc_nServoMin) / 2;
    ms_nServoDesired = ms_nCenterPos;
    f_servoCenter.attach(gsc_pinServo1);
    vMoveServoTo(ms_nServoDesired);
}

void CCenterServo::vUpdateCenterServo(SColorReading * p_pColorReadingLeft, SColorReading * p_pColorReadingRight)
{
  int t_nDir = 0;
  if (ms_nServoDesired < 180 && p_pColorReadingRight->m_r > fsc_nMinRed)
  {
    t_nDir = fsc_nSpeed;
  }
  
  if (ms_nServoDesired > 0 && p_pColorReadingLeft->m_r > fsc_nMinRed)
  {
    t_nDir = -fsc_nSpeed;
  }

  ms_nServoDesired += t_nDir;
  
  if (ms_nCenterPos != ms_nServoDesired)
  {
    vMoveServoTo(ms_nServoDesired); 
  }
}

void CCenterServo::vMoveServoTo(int p_nPos)
{
  ms_nCenterPos = constrain(p_nPos, msc_nServoMin, msc_nServoMax);
  ms_nServoDesired = ms_nCenterPos;

  if (CButton::ms_nButtonState == LOW )
  {
    Serial.print("Moving Center Servo To : ");
    Serial.println(ms_nCenterPos);
    f_servoCenter.write(ms_nCenterPos);
  }
}

