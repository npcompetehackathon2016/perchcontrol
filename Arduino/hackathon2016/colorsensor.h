#ifndef _HACKATHON_2016_COLOR_SENSOR_H_
#define _HACKATHON_2016_COLOR_SENSOR_H_

#include <Wire.h>
#include "Adafruit_TCS34725.h"

struct SColorReading
{
  uint16_t m_r;
  uint16_t m_g;
  uint16_t m_b;
  uint16_t m_clear;
};

class CColorSensor
{
  public:
  void vSetup(uint8_t p_nTcaSelect);
  void vGetColorReadingRaw(SColorReading * p_pColorReading);
  void vGetColorReadingHex(SColorReading * p_pColorReading, bool p_fPrint = false);

  Adafruit_TCS34725 m_sensor;
  uint8_t m_nTcaSelect;
};


#endif
