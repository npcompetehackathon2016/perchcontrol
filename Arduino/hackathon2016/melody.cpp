#include "melody.h"
#include "pinlayout.h"


void vPlayMelody1()
{
  int t_aMelody[] = {NOTE_D5, NOTE_E5, NOTE_FS5};
  int t_aTiming[] = {250, 150, 650};

  vPlayMelody(sizeof(t_aMelody)/sizeof(int), t_aMelody, t_aTiming);
}

void vPlayMelody2()
{
  int t_aMelody[] = {NOTE_FS5, NOTE_D5, NOTE_FS5};
  int t_aTiming[] = {125, 125, 125};
  vPlayMelody(sizeof(t_aMelody)/sizeof(int), t_aMelody, t_aTiming);  
}

void vPlayMelody(int p_cElements, int * p_paMelody, int * p_paTime)
{
  for (int t_iNote = 0; t_iNote < p_cElements; ++t_iNote)
  {
    int t_nNote = p_paMelody[t_iNote];
    tone(gsc_pinSpeaker, t_nNote, p_paTime[t_iNote]);
    delay(p_paTime[t_iNote] * 1.01);
  }
}
