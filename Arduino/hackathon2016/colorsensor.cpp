#include "colorsensor.h"
#include "i2cmultiplexer.h"

void CColorSensor::vSetup(uint8_t p_nTcaSelect)
{
  m_nTcaSelect = p_nTcaSelect;
  vTcaSelect(m_nTcaSelect);
  m_sensor = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_60X);

  while (m_sensor.begin() == false)
  {
    Serial.println("Searching for Color Sensor");
    delay(10);
    vTcaSelect(m_nTcaSelect);
  }
}

void CColorSensor::vGetColorReadingRaw(SColorReading * p_pColorReading)
{
  vTcaSelect(m_nTcaSelect);
  m_sensor.getRawData(&p_pColorReading->m_r, &p_pColorReading->m_g, &p_pColorReading->m_b, &p_pColorReading->m_clear); 
}

void CColorSensor::vGetColorReadingHex(SColorReading * p_pColorReading, bool p_fPrint)
{
  vGetColorReadingRaw(p_pColorReading);
  float t_r = (float)p_pColorReading->m_r / p_pColorReading->m_clear;
  float t_g = (float)p_pColorReading->m_g / p_pColorReading->m_clear;
  float t_b = (float)p_pColorReading->m_b / p_pColorReading->m_clear;
  p_pColorReading->m_r = t_r * 256;
  p_pColorReading->m_g = t_g * 256;
  p_pColorReading->m_b = t_b * 256;

  if (p_fPrint)
  {
    Serial.print("C:\t"); Serial.print(p_pColorReading->m_clear);
    Serial.print("\tR:\t"); Serial.print(p_pColorReading->m_r);
    Serial.print("\tG:\t"); Serial.print(p_pColorReading->m_g);
    Serial.print("\tB:\t"); Serial.print(p_pColorReading->m_b);
    Serial.println("");
  }
}

