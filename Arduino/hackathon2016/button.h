#ifndef _HACKATHON_2016_BUTTON_H_
#define _HACKATHON_2016_BUTTON_H_

#include <Arduino.h>

class CButton
{
  public:
  static const int msc_nButtonBrightOn = 255;
  static const int msc_nButtonBrightOff = 80;
  
  static void vUpdateButtonState();
  static void vSetButtonBrightness(int p_nBrightness);

  static int ms_nButtonState;
  static int ms_nButtonBrightCur;
};

#endif
