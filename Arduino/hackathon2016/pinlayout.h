#ifndef _HACKATHON_2016_PIN_LAYOUT_H_
#define _HACKATHON_2016_PIN_LAYOUT_H_

//Pin Defines
static const int gsc_pinServo1 = 10;
static const int gsc_pinPushButton = 4;
static const int gsc_pinPushButtonLED = 3;
static const int gsc_pinSpeaker = 5;

#endif
