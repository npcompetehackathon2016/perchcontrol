#ifndef _I2C_MULTIPLEXER_H_
#define _I2C_MULTIPLEXER_H_

//Using Adafruit TCA9548A
#define TCAADDR 0x70


static void vTcaSelect(uint8_t p_iChannel)
{
  static const int tsc_nMaxChannel = 7;
  if (p_iChannel > tsc_nMaxChannel)
  {
    return;
  }

  delay(1);
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << p_iChannel);
  Wire.endTransmission();  
}

#endif
